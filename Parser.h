#ifndef PARSER_H
#define PARSER_H
#include <iostream>
#include <string>
#include <vector> // like arrayList
#include "CommandWords.h"
#include "Command.h"
using namespace std;

class Parser
{
  private:
    string makeLowerCase();//prevents reader error
    CommandWords commands;
  public:
  Parser();
  Command getCommand();
  string word1;
  string word2;
  string inputLine;
  void showCommands();
  string makeLowerCase(string input);
};
#endif

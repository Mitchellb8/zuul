/**
*
* Runs the zuul game
**/
#include <iostream>
#include "CommandWords.h"
#include "Room.h"
#include "Command.h"
#include "Parser.h"
#include "Game.h"
using namespace std;
int main()
{
	Game theGame;
	theGame.play();
}

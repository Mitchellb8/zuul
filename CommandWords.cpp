#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "Commandwords.h"
using namespace std;

/*
CommandWords deals with the first word entered by a user
constructs a vector of comprehendable inputs and test an input
to see if can be understood
*/
CommandWords::CommandWords()
{
  validCommands.push_back("go");
  validCommands.push_back("quit");
  validCommands.push_back("help");
}
/*
isCommand tests an input to see if it matches a valid command that
can be processed
*/
bool CommandWords::isCommand(string aString)
{
  bool returnBool = false;
  if("go" == aString || "quit" == aString || "help" == aString)
  {
    returnBool = true;
  }
  return returnBool;
}
//outputs all valid inputs **valid 1st words
void CommandWords::showAll()
{
  for(int i = 0; i < validCommands.size(); i++)
  {
    cout << validCommands[i] + ", " ;
  }
  cout << "are valid commands" << endl;
}

#include <iostream>
#include <string>
#include <vector> // like arrayList
#include <set> //like hashset
#include <map> // like hash map
#include <algorithm>
#include "Room.h"
using namespace std;
//default constructor class. Basically just to avoid errors
Room::Room()
{
  description = "";
}
//constructor that will be invoked in Game.
Room::Room(string descriptionIn)
{
  description = descriptionIn;
}
// used to assign the exits map all the valid exits to a room
void Room::setExit(string direction, Room * neighbor)
{
  exits[direction] = neighbor;
}
//returns a discription of the room such as "in a lecture hall"
string Room::getShortDescription()
{
  return description;
}
//returns short discription plus all possible exit directions
string Room::getLongDescription()
{
  return ("You are " + description + " " + getExitString());
}
//returns all possible exits this room may have
string Room::getExitString()
{
  string returnString = "There is an exit: ";
  if(exits.find("west") != exits.end())
  {
    returnString = returnString + "To The West: ";
  }
  if (exits.find("east") != exits.end())
  {
    returnString = returnString + "To The East: ";
  }
  if (exits.find("north") != exits.end())
  {
    returnString = returnString + "To The North: ";
  }
  if (exits.find("south") != exits.end())
  {
    returnString = returnString + "To The South: ";
  }
  return returnString;
}
// returns a room given a direction. only invoked if there is a room
Room * Room::getExit(string direction)
{
    return exits[direction];
}
/*
given a direction validExit will detemine if there is a room there
invoked before get exit to prevent errors
*/
bool Room::validExit(string direction)
{
  bool returnBoolean = false;
  if(exits.find(direction) != exits.end())
  {
    returnBoolean = true;
  }
  return returnBoolean;
}

#ifndef ROOM_H
#define ROOM_H
#include <iostream>
#include <string>
#include <vector> // like arrayList
#include <set> //like hashset
#include <map> // like hash map
using namespace std;

class Room
{
private:
  string description;
  map<string, Room *> exits;
  string getExitString();
public:
  Room();
  Room(string discription);
  void setExit(string direction, Room * neighbor);
  string getShortDescription();
  string getLongDescription();
  Room * getExit(string direction);
  bool validExit(string direction);
};
 #endif

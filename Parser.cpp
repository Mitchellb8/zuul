#include <iostream>
#include <string>
#include <vector> // like arrayList
#include <map> // like hash map
#include <algorithm>
#include <cctype>
#include "Parser.h"
#include "CommandWords.h"
#include "Command.h"
using namespace std;

/*
class that directly deals with user inputs. Process them to make it
readable and then passes the words off to the proper
classes(i.e. Game, CommandWords)
*/
Parser::Parser()
{
/*
  cout << "> ";
  bool in = false;
  while (!in)
  {
    if(cin.get() != '\n')
    {
      in = true;
    }
  }
  getCommand();
  */
}
/*
"main" class for Parser. Uses cin to obtain an input,
makeLowerCase to prevent errors from capital letters,
seperates the input into 2 words, then passes it off to
CommandWords and Command to further process it
*/
Command Parser::getCommand()
{
  CommandWords commands;
  string inputLine;
  getline(cin, inputLine);
  inputLine = makeLowerCase(inputLine);
  string command = " ";//will be assigned the 1st input word
  string secondCommand = " ";//will be assigned 2nd input word
  int location = inputLine.find_first_of(" ");
  if (location >= 1)
  {
    command = inputLine.substr(0,location);
    if (command != " ")
    {
      secondCommand = inputLine.substr(location + 1);
  } 
  }else{
    command = inputLine;
    secondCommand = " ";
  }
  if(!commands.isCommand(command))
  {
    command = "";
  }
  const Command commandReturn(command, secondCommand);
  return commandReturn;
}
//makes the user input all lower case characters to prevent error
string Parser::makeLowerCase(string input)
{
  tolower('A');
  transform(input.begin(), input.end(), input.begin(), ::tolower);
  return input;
}
//returns all valid commandWords
void Parser::showCommands()
{
  commands.showAll();
}

#include <iostream>
#include <string>
#include <vector>
#include <map> // like hash map
#include "Game.h"
#include "Room.h"
#include "Command.h"
#include "Parser.h"
#include "CommandWords.h"
using namespace std;
/*
Game() is the constructor for game class
once called it creates the room map
the main function the calls play()
*/
Game::Game()
{
  Game::createRooms();
}
//creates the map rooms and fills it with all the rooms in zuul
void Game::createRooms()
{

  rooms["outside"] = new Room("outside the main enterance to the university");
  rooms["theater"] = new Room("in a lecture hall");
  rooms["pub"] = new Room("in the campus pub");
  rooms["lab"] = new Room("in a computing lab");
  rooms["office"] = new Room("in the computing admin office");

  rooms["outside"]->setExit("east", rooms["theater"]);
  rooms["outside"]->setExit("south", rooms["lab"]);
  rooms["outside"]->setExit("west", rooms["pub"]);

  rooms["theater"]->setExit("west", rooms["outside"]);

  rooms["pub"]->setExit("east", rooms["outside"]);

  rooms["lab"]->setExit("north", rooms["outside"]);
  rooms["lab"]->setExit("east", rooms["office"]);

  rooms["office"]->setExit("west", rooms["lab"]);
  currentRoom = rooms["outside"];//currentRoom is always set to the current room
}
/*
main class for Game
runs the entire game until finished = true
*/
void Game::play()
{
  //Game::createRooms();
  Game::printWelcome();
  bool finished = false;
  while (!finished)
  {
    Parser parser;
    Command command = parser.getCommand();
    finished = processCommand(command);
  }
  cout << ("Thank you for playing!  Good Bye!") << endl;
}
//prints a standard welcome string
void Game::printWelcome()
{
  cout << (" ")<< endl;
  cout << ("Welcome to the World of Zuul!")<< endl;
  cout << ("World of Zuul is a new, incredibly boring adventure game")<< endl;
  cout << ("Type 'help' if you need help")<< endl;
  cout << (" ")<< endl;
  cout << (currentRoom->getLongDescription())<< endl;
}
//determines what to do when a command is entered
bool Game::processCommand(Command command)
{
  bool wantToQuit = false;

  if(command.isUnknown())
  {
    cout << ("I don't know what you mean") << endl;
  }

  string commandWord = command.getCommandWord();
  if(commandWord == ("help"))
  {
    printHelp();
  }else if (commandWord == ("go"))
  {
    goRoom(command);
  }else if (commandWord == ("quit"))
  {
    wantToQuit = quit(command);
  }
  return wantToQuit;
}
/*
outputs the context of the game and possible enteries when a user
enters help
*/
void Game::printHelp()
{
  cout << ("You are lost. You are alone. You wander") << endl;
  cout << ("around at the university. ") << endl;
  cout << (" ") << endl;
  cout << ("Your command words are: ") << endl;
  parser.showCommands();
}
/*
determines if a user input instructing a user to change rooms
is valid and then if it is it changes the room through currentRoom
if its not it returns "Theres no Door!"
*/
void Game::goRoom(Command command)
{
  if(!command.hasSecondWord())
  {
    cout << ("Go Where? ") << endl;
    return;
  }
  string direction = command.getSecondWord();
  //Room * nextRoom;
  if(currentRoom->validExit(direction))
  {
    currentRoom = currentRoom->getExit(direction);
    cout << (currentRoom->getLongDescription()) << endl;
  } else {
    cout << ("There is no door! ") << endl;
  }
}
/*
determines when to end the game based on a user's input
invoked when the 1st word a user enters is 'quit'
*/
  bool Game::quit(Command command)
  {
    if(command.hasSecondWord())
    {
      cout << ("Quit what?") << endl;
      return false;
    } else {
      return true;
    }
  }

#ifndef GAME_H
#define GAME_H

#include <iostream>
#include <string>
#include <vector> // like arrayList
#include <set> //like hashset
#include <map> // like hash map
#include "Command.h"
#include "Room.h"
#include "Parser.h"
#include "CommandWords.h"

using namespace std;

class Game
{
private:
  Parser parser;
  Room * currentRoom;
  map<string, Room*> rooms;
  void createRooms();
  void printWelcome();
  bool processCommand(Command command);
  void printHelp();
  void goRoom(Command command);
  bool quit(Command command);
public:
  Game();
  void play();
};
#endif

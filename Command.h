#ifndef COMMAND_H
#define COMMAND_H
#include <iostream>
#include <string>
using namespace std;

class Command
{
  private:
    std::string commandWord;
    std::string secondWord;
  public:
    Command();
    Command(string firstWord, string secondWord);
    string getCommandWord();
    string getSecondWord();
    bool isUnknown();
    bool hasSecondWord();
};
#endif

#include <iostream>
#include <string>
#include "Command.h"
using namespace std;

Command::Command()
{
  //default constructor to prevent error
}
/*
the constructor that will always be used
assignes the input words' value
*/
Command::Command(string firstWord, string second_Word)
{
  commandWord = firstWord;
  secondWord = second_Word;
}
//returns the first word entered by the user
string Command::getCommandWord()
{
  return commandWord;
}
//returns the second word entered by a user
string Command::getSecondWord()
{
  return secondWord;
}
//tests if commandWord(1st word in user input) matches a CommandWord
bool Command::isUnknown()
{
    if (commandWord == "") {
      return true;
    }else{
      return false;
    }
}
//determines if a second input word is present
bool Command::hasSecondWord()
{
  int i = secondWord.size();
  if (i > 0)
  {
    return true;
  }else{
    return false;
  }
}

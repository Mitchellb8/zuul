#ifndef COMMANDWORDS_H
#define COMMANDWORDS_H
#include <iostream>
#include <string>
#include <vector> // like arrayList
#include <set> //like hashset
#include <map> // like hash map
using namespace std;

class CommandWords
{
private:
  std::vector<std::string> validCommands;
public:
  CommandWords();
  bool isCommand(std::string aString);
  void showAll();
};
#endif
